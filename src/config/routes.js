const express = require('express')

module.exports = function(server) {
  const router = express.Router() // gets the express router
  server.use('/api', router) // middleware to use our _router in case of requests from /api

  const todoService = require('../api/todo/todoService') // gets our todoService
  todoService.register(router, '/todos') // every url base that matches 'todos' will create a webservice with the base methods (get, post, put, delete)
}
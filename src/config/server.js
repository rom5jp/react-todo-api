const express = require('express')
const server = express()
const bodyParser = require('body-parser')
const allowCors = require('./cors')

const port = 3003

server.use(bodyParser.urlencoded({ extended: true })) // middleware to handle encoded requests
server.use(bodyParser.json()) // middleware to handle json requests
server.use(allowCors)

server.listen(port, function() {
  console.log(`Server is running on port ${port}`);
})

module.exports = server
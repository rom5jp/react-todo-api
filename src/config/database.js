const mongoose = require('mongoose')

mongoose.Promise = global.Promise // work with Promise from the Node Promise API

module.exports = mongoose.connect('mongodb://localhost/todo')
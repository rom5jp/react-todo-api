const Todo = require('./todo')

Todo.methods(['get', 'post', 'put', 'delete'])
Todo.updateOptions(
  { 
    new: true, // necessary to retrieve the modified object from db
    runValidators: true 
  }
) 

module.exports = Todo